# ClassBuilder

Gerador de classes PHP baseado em tabelas MySQL
Este pacote pode ser usado para gerar classes PHP capazes de armazenar e recuperar registros da tabela do banco de dados MySQL

Sobre
O pacote ClassBuilder.php reune neste único arquivo, um conjunto de classes que irão extrair as tabelas do seu banco de dados MySQL e, a partir disto, criar correspondentes classes PHP. Pode ser de grande ajuda para quem gosta de trabalhar com este modelo de Orientação a Objetos.

Cada classe PHP gerada possui métodos/funções get() e set(), além dos metodos Save() e Create(), que permite manipular os atributos (variáveis) individualmente, além claro, de poder salvar, incluir ou consultar direto no banco de dados de forma transparente, sem criar qualquer SQL.

Como funciona?
Em resumo a ideia é pegar uma tabela MySQL e criar uma classe PHP. Mas pra que serve isto? Bem, isto torna possível manipular um objeto da classe afetando direto os registros do banco de dados.

Rode pelo navegador o script ClassBuilder.php e vai ser gerado na tela o código fonte para a classe “cliente”. Não se assuste se parecer embolado! Para melhorar a visualização, clique com o botão direito do mouse e escolha a opção “Exibir código fonte” ou “Código fonte”.


FONTE: TAYLOR LOPES BLOG (www.taylorlopes.com)